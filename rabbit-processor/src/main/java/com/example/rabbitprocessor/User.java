package com.example.rabbitprocessor;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;
import lombok.experimental.Wither;

@Value
@Wither
public class User {
    @JsonProperty("id")
    private Integer id;

    @JsonProperty("contact_number")
    private String contactNumber;

    @JsonProperty("country_code")
    private String countryCode;

    @JsonProperty("password")
    private String password;
}
