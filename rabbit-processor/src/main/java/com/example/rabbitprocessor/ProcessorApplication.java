package com.example.rabbitprocessor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Processor;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.SendTo;

@SpringBootApplication
@EnableBinding(Processor.class)
public class ProcessorApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProcessorApplication.class, args);
    }

    @StreamListener(Processor.INPUT)
    @SendTo(Processor.OUTPUT)
    public User processUser(User user) {
        System.out.println("Processing user: " + user);
        return user.withId(2)
                   .withContactNumber("12345678");
    }


    // process with exception, message should be rethrown back into dead letter queue
//    @StreamListener(Processor.INPUT)
//    @SendTo(Processor.OUTPUT)
//    public User processUserWithError(User user) {
//        AtomicBoolean failLatch = new AtomicBoolean(true);
//        System.out.println("Processing user with error: " + user);
//        if(failLatch.get()) throw new RuntimeException("adsf");
//        return user.withId(2)
//                   .withContactNumber("12345678");
//    }

    @StreamListener("errorChannel")
    public void error(Message<?> message) {
        System.out.println("Handling error: " + message);
    }
}