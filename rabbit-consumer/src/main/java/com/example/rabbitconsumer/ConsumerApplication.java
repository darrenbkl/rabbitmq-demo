package com.example.rabbitconsumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.messaging.Message;


@SpringBootApplication
@EnableBinding(Sink.class)
public class ConsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConsumerApplication.class, args);
    }

    @StreamListener(Sink.INPUT)
    public void consumeUser(User user) {
        System.out.println("Consuming user: " + user);
    }

    // process with exception, message should be rethrown back into dead letter queue
//	@StreamListener(Sink.INPUT)
//	public void consumeUserWithError(User user) {
//		System.out.println("Consuming user with error: " + user);
//        throw new RuntimeException("adsf");
//	}

    @StreamListener("errorChannel")
    public void error(Message<?> message) {
        System.out.println("Handling error: " + message);
    }
}