package com.example.rabbitproducer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@SpringBootApplication
@EnableBinding(Source.class)
public class ProducerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProducerApplication.class, args);
    }

    @RestController
    @Configuration
    static class ctr {
        @Autowired Source source;

        @GetMapping("/send")
        public void sendMessage() {
            User user = new User(1, "87654321", "65", "password");
            System.out.println("Publishing user: " + user);
            source.output()
                  .send(new GenericMessage<>(user));
        }
    }

    @StreamListener("errorChannel")
    public void error(Message<?> message) {
        System.out.println("Handling error: " + message);
    }
}